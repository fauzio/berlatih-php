<?php
function ubah_huruf($string){
//kode di sini
$abjad = "abcdefghijklmnopqrstuvwxyza";
$hasilmapping="";
for($i=0;$i<strlen($string);$i++){
    $pengganti=strpos($abjad,$string[$i])+1;
    $hasilmapping=$hasilmapping . $abjad[$pengganti];
}
return "$hasilmapping <br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>